const original = ['Rojo', 'Verde', 'Azul', 'Amarillo'];
// -------
let indiceAmarillo = null;
let cantidadColoresConA = 0;
let arregloSinAzul = [];
let ordenadoPorCantidadLetras = [];
let arregloConNuevoColorInicio = [];
let arregloConNuevoColorFin = [];

const buscarIndice = () => {
  indiceAmarillo = null;
  for (let i = 0; i < original.length; i++) {
    if (original[i] === 'Amarillo') {
      indiceAmarillo = i;
      return indiceAmarillo;
    }
  }
  return indiceAmarillo;
}

const contarColoresConA = () => {
  cantidadColoresConA = 0;

  for (let i = 0; i < original.length; i++) {
    for(let j = 0; j < original[i].length; j++) {
      if(original[i][j].toLowerCase() === 'a') {
        cantidadColoresConA++;
      }
    }
  }

  return cantidadColoresConA;
}

const filterArregloSinAzul = () => {
  let arregloSinAzul = original.filter((el) => el !== 'Azul');
  return arregloSinAzul;
}

const ordenarPorCantidadLetras = () => {
  ordenadoPorCantidadLetras = original.sort((a, b) => a.length - b.length);
  return ordenadoPorCantidadLetras;
}

const agregarNuevoColorInicio = (nuevoColor) => {
  arregloConNuevoColorInicio = original.unshift(nuevoColor);
  return arregloConNuevoColorInicio;
}

indiceAmarillo = buscarIndice();
cantidadColoresConA = contarColoresConA();
arregloSinAzul = filterArregloSinAzul();
ordenadoPorCantidadLetras = ordenarPorCantidadLetras();
arregloConNuevoColorInicio = agregarNuevoColorInicio('Red');

console.log(indiceAmarillo);
console.log(cantidadColoresConA);
console.log(arregloSinAzul);
console.log(ordenadoPorCantidadLetras)
console.log(arregloConNuevoColorInicio)